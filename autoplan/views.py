﻿from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
# from .models import Po, Invoice, InvoiceItems, Supplier, PoItems
from .models import Knowledgelist, Initialgoalknowledge, Result, Courselist, Postknowledge
from django.db.models import Count
import logging
import subprocess
import random

logging.basicConfig(filename='test.log', level=logging.DEBUG)

errorLog = []


def index(request):
    logging.debug("In function index")
    knowledgelist = Knowledgelist.objects.all().order_by('name')
    courselist = Courselist.objects.all().order_by('name')
    # for knw in knowledgelist:
    # logging.debug(knw.__str__())

    context = {
        'knowledgelist': knowledgelist,
        'courselist': courselist,
    }
    return render(request, 'po/multi-upload.html', context)


def error_log(request):
    logging.debug("In function index")
    context = {
        'errorLog': errorLog,
    }
    return render(request, 'po/error-logs.html', context)


def supplier_report(request):
    # all_suppliers = Supplier.objects.all()
    # for sup in all_suppliers:
    #     logging.debug(sup.__str__())

    context = {
        # 'all_suppliers': all_suppliers,
    }
    return render(request, 'po/data-table-supplier.html', context)


def purchase_order_report(request):
    # all_po = Po.objects.annotate(number_of_items=Count('poitems') + 2)
    context = {
        #     'all_po': all_po,
    }
    return render(request, 'po/course-plan.html', context)


def has_common_member(a, b):
    a_set = set(a)
    b_set = set(b)
    if a_set & b_set:
        return True
    else:
        return False


# rather than knowledge name list, knowledge id list should be the second arguement.
def write_initialgoalknowledge_to_table(transactID, iknwdList, iiorg):
    aListOfKnowledge = []
    for knwldName in iknwdList:
        knwdListObj = Knowledgelist.objects.get(name=knwldName)
        logging.debug(knwdListObj.knowledgeid)
        logging.debug(knwdListObj.name)
        logging.debug(knwldName)
        aListOfKnowledge.append(Initialgoalknowledge(transactionid=transactID, iorg=iiorg, knowledgeid=knwdListObj))

    logging.debug(aListOfKnowledge)
    Initialgoalknowledge.objects.bulk_create(aListOfKnowledge)


def write_initialgoalknowledge_to_table_from_courses(transactID, icourseList, iiorg):
    aListOfKnowledge = []
    for courseName in icourseList:
        logging.debug("course name is : ")
        logging.debug(courseName)
        courseListObj = Courselist.objects.get(name=courseName)
        postKnwldObjectList = Postknowledge.objects.filter(courseid=courseListObj.courseid)
        for postKnwldObject in postKnwldObjectList:
            logging.debug("knowledge id is : ")
            logging.debug(postKnwldObject.knowledgeid.knowledgeid)
            knwdListObj = Knowledgelist.objects.get(pk=postKnwldObject.knowledgeid.knowledgeid)
            aListOfKnowledge.append(
                Initialgoalknowledge(transactionid=transactID, iorg=iiorg, knowledgeid=knwdListObj))

    logging.debug(aListOfKnowledge)
    Initialgoalknowledge.objects.bulk_create(aListOfKnowledge)


def show_result(request):
    logging.debug('In function show_result')
    lCourseObj = []
    transactionID = -1
    retVal = False
    if request.method == 'POST':
        initials = request.POST.getlist('initials')
        logging.debug('Selected Initial Knowledge is : ')
        logging.debug(initials)
        goals = request.POST.getlist('knowledges')
        logging.debug('Selected Goal Knowledge is : ')
        logging.debug(goals)
        try:
            # check initial and goal are different
            if initials and goals:  # both lists have something
                initials.append('High School Diploma')  # always append 'High School Diploma'
                if not has_common_member(initials, goals):
                    Initialgoalknowledge.objects.all().delete()
                    # generate a random number for transactionID
                    transactID = random.randint(10 ** 3, 10 ** 4)
                    # write goal and initial to initialgoal knowledge table
                    logging.debug("before write_initialgoalknowledge_to_table_from_courses")
                    write_initialgoalknowledge_to_table_from_courses(transactID, initials, 'I')
                    logging.debug("after initial knowledge insert function")
                    write_initialgoalknowledge_to_table(transactID, goals, 'G')
                    logging.debug("after goal knowledge insert function")
                    retVal = True
                    # call jar file
                    subprocess.call(['java', '-jar', 'c:\\AutoPlan\\AutoPlan.jar', str(transactID)])
                    logging.debug("after AutoPlan.jar")
                    # read result table
                    resultList = Result.objects.filter(transactionid=transactID).order_by('stage')  # arrange by stage
                    logging.debug(resultList)


                    for res in resultList:
                        logging.debug(res)
                        logging.debug(res.courseid)
                        logging.debug(res.stage)
                        # course = Courselist.objects.get(courseid=res.courseid)  # from course id - fetch values from courselist table
                        course = Courselist.objects.get(courseid=res.courseid.courseid)
                        course.stage = res.stage
                        transactionID = res.transactionid
                        lCourseObj.append(course)

        except Exception as e:
            logging.getLogger("error_logger").error("Unable to execute iCourseplanner. " + repr(e))
            return upload_datafile(request)
    if retVal:
        context = {
            'all_courses': lCourseObj,
            'transactionID': transactionID
        }
        return render(request, 'po/course-plan.html', context)
    else:
        return upload_datafile(request)


def search_invoice(request):
    lInvObj = []
    if request.method == 'POST':
        search_id = request.POST.get('textfield', None)
        logging.debug('Search ID is : ')
        logging.debug(search_id)
        try:
            # inObj = Invoice.objects.get(in_number=search_id)
            # inItemsList = list(InvoiceItems.objects.filter(in_number=inObj.in_number))
            # inObj.number_of_items = len(inItemsList) + 2
            # lInvObj.append(inObj)
            context = {
                'all_invoices': lInvObj,
            }
            return render(request, 'po/data-table.html', context)
        except Exception as e:
            context = {
                'all_invoices': lInvObj,
            }
            return render(request, 'po/data-table.html', context)
    else:
        context = {
            'all_invoices': lInvObj,
        }
        return render(request, 'po/data-table.html', context)


def outstanding_items_report(request):
    context = {}
    return render(request, 'po/data-table-outstanding.html', context)


def invoice_report(request):
    logging.debug('IN function new invoice_report ')
    context = {
        'success': 'success',
    }
    if request.method == 'POST':
        logging.debug('POST method detected')
        # if 'pieFact' in request.POST:
        initial = request.POST.getlist('initial[]')
        # pieFact = request.POST['pieFact']
        # doSomething with pieFact here...
        logging.debug('Selected items are : ')
        logging.debug(initial)
        goal = request.POST.getlist('goal[]')
        logging.debug(goal)
        # check initial and goal are different

        # write goal and initial to initialgoal knowledge table
        # call jar file
        # read result table
        # arrange by stage
        # from course id - fetch values from courselist table
        # send new view
        return render_to_response("po/data-table-supplier.html", context)  # if everything is OK
    # nothing went well

    return render_to_response("po/data-table-supplier.html", context)


def upload_datafile(request):
    logging.debug('IN function new upload_datafile ')
    knowledgelist = Knowledgelist.objects.all()
    # for knw in knowledgelist:
    # logging.debug(knw.__str__())

    context = {
        'knowledgelist': knowledgelist,
    }
    return render(request, 'po/multi-upload.html', context)
