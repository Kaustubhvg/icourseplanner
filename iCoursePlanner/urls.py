from django.contrib import admin
from django.urls import path
from autoplan.views import index, upload_datafile, invoice_report, purchase_order_report, supplier_report, \
    outstanding_items_report, error_log, show_result, search_invoice
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'po/', index, name="index"),
    path(r'upload/', upload_datafile, name="upload_datafile"),
    path(r'invoice/', invoice_report, name="invoice_report"),
    path(r'supplier', supplier_report, name="supplier_report"),
    path(r'purchase_order/', purchase_order_report, name="purchase_order_report"),
    path(r'outstanding_items/', outstanding_items_report, name="outstanding_items_report"),
    path(r'error_log/', error_log, name="error_log"),
    path(r'show_result/', show_result, name="show_result"),
    path(r'search_invoice/', search_invoice, name="search_invoice"),

    path(r'index', index, name="index"),
    path(r'', index, name="index"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
