﻿# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models



class Courselist(models.Model):
    courseid = models.CharField(db_column='courseID', primary_key=True, max_length=10)  # Field name made lowercase.
    name = models.CharField(max_length=100, blank=True, null=True)
    credithours = models.FloatField(db_column='creditHours', blank=True, null=True)  # Field name made lowercase.
    tuition = models.FloatField(blank=True, null=True)
    provider = models.CharField(max_length=50, blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'courselist'



class Initialgoalknowledge(models.Model):
    transactionid = models.BigIntegerField(db_column='transactionID', primary_key=True)  # Field name made lowercase.
    iorg = models.CharField(max_length=10)
    knowledgeid = models.ForeignKey('Knowledgelist', models.DO_NOTHING, db_column='knowledgeID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'initialgoalknowledge'
        unique_together = (('transactionid', 'iorg', 'knowledgeid'),)


class Knowledgelist(models.Model):
    knowledgeid = models.CharField(db_column='knowledgeID', primary_key=True, max_length=10)  # Field name made lowercase.
    name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'knowledgelist'


class Postknowledge(models.Model):
    courseid = models.ForeignKey(Courselist, models.DO_NOTHING, db_column='courseID', primary_key=True)  # Field name made lowercase.
    knowledgeid = models.ForeignKey(Knowledgelist, models.DO_NOTHING, db_column='knowledgeID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'postknowledge'
        unique_together = (('courseid', 'knowledgeid'),)


class Preknowledge(models.Model):
    courseid = models.ForeignKey(Courselist, models.DO_NOTHING, db_column='courseID', primary_key=True)  # Field name made lowercase.
    knowledgeid = models.ForeignKey(Knowledgelist, models.DO_NOTHING, db_column='knowledgeID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'preknowledge'
        unique_together = (('courseid', 'knowledgeid'),)


class Result(models.Model):
    transactionid = models.BigIntegerField(db_column='transactionID', primary_key=True)  # Field name made lowercase.
    stage = models.IntegerField()
    courseid = models.ForeignKey(Courselist, models.DO_NOTHING, db_column='courseID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'result'
        unique_together = (('transactionid', 'stage', 'courseid'),)
