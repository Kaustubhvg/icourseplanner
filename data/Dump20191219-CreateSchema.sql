CREATE DATABASE  IF NOT EXISTS `autoplan` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `autoplan`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: localhost    Database: autoplan
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `careerpath`
--

DROP TABLE IF EXISTS `careerpath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `careerpath` (
  `careerPathID` char(4) NOT NULL,
  `careerPathName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`careerPathID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `careerpath`
--

LOCK TABLES `careerpath` WRITE;
/*!40000 ALTER TABLE `careerpath` DISABLE KEYS */;
INSERT INTO `careerpath` VALUES ('C100','Data Analyst');
/*!40000 ALTER TABLE `careerpath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courselist`
--

DROP TABLE IF EXISTS `courselist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courselist` (
  `courseID` varchar(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `creditHours` float DEFAULT NULL,
  `tuition` float DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `provider` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`courseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courselist`
--

LOCK TABLES `courselist` WRITE;
/*!40000 ALTER TABLE `courselist` DISABLE KEYS */;
INSERT INTO `courselist` VALUES ('ICP000','High School Diploma',0,1,1,'High School',NULL),('ICP001','Calculus 1',4,1,0,'',''),('ICP002','Calculus 2',4,1,0,'',''),('ICP003','Calculus 3',4,1,0,'',''),('ICP004','Physics 1',4,1,0,'',''),('ICP005','Physics 2',4,1,0,'',''),('ICP006','Introduction to Linear Algebra',3,1,0,'',''),('ICP100','Introduction to Geometry',0,1,14,'edX','https://www.edx.org/course/introduction-geometry-schoolyourself-geometryx-1'),('ICP103','Computer Aided Graphics',2,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP110','Introduction to Computers & Computational Analysis',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP148','Information Engineering',0,1,0,'Iowa State',''),('ICP200','Introduction to Python: Fundamentals',0,1,5,'edX','https://www.edx.org/course/introduction-to-python-fundamentals-1'),('ICP222','Design & Analysis Methods for System Improvements',3,1,0,'Iowa State','N/A'),('ICP231','Probability and Statistical Inference for Engineers',0,1,0,'Iowa State',''),('ICP248','Engineering System Design Manufacturing Processes and Specifications',3,1,0,'Iowa State','N/A'),('ICP271','Applied Ergonomics and Work Design',3,1,0,'Iowa State','https://www.coursehero.com/file/27974709/IE-271-Syllabus-SP2018pdf/'),('ICP300','Introduction to Statistics',0,1,6,'Coursera','https://www.coursera.org/learn/probability-statistics'),('ICP301','Engineering Economy I',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP302','Introduction to Quality Engineering',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP305','Engineering Economy II',2,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP311','Introduction to Engineering Statistical Methods',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP313','Operations Research I',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP314','Operations Research II',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP331','Fundamentals of Materials Science',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP333','Materials Science Laboratory',2,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP341','Introduction to Manufacturing Processes',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP342','Production Systems',3,1,0,'Iowa State',''),('ICP348','Solidification Processes',3,1,0,'Iowa State','http://www.public.iastate.edu/~fpeters/Peters_IE348.htm'),('ICP361','Introduction to Simulation and Expert Systems',2,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP385','Introduction to Logistics and Supply Chain',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP386','Industrial and Managerial Engineering',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP395','Solid Modeling & Rapid Prototyping',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP400','Autonomous Navigation for Flying Robots',0,1,8,'edX','https://www.edx.org/course/autonomous-navigation-flying-robots-tumx-autonavx-0'),('ICP403','Introduction to Sustainable Production Systems',3,1,0,'Iowa State','https://courses.elo.iastate.edu/spring/2015/I%20E/403/XE/overview'),('ICP412','Design and Analysis of Experiments',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP422','Manufacturing Quality Control',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP431','Materials Engineering',2,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP432','Industrial Automation',3,1,0,'Iowa State',''),('ICP441','Industrial Engineering Design',3,1,0,'Iowa State',''),('ICP443','Manufacturing Processes II',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP445','Computer Aided Manufacturing',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP446','Geometric Variability in Manufacturing',3,1,0,'Iowa State',''),('ICP447','Biomedical Design and Manufacturing',3,1,0,'Iowa State',''),('ICP448','Manufacturing Systems Engineering',3,1,0,'Iowa State','http://www.elo.iastate.edu/files/2017/05/IE-448-Syllabus-S17.pdf'),('ICP449','Computer Aided Design and Manufacturing',3,1,0,'Iowa State',''),('ICP461','Simulation of Manufacturing and Service Systems',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP465','Multidisciplinary Engineering Design I',3,1,0,'Iowa State',''),('ICP466','Facilities Planning',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP467','Multidisciplinary Engineering Design II',3,1,0,'Iowa State',''),('ICP468','Large-Scale Complex Engineered Systems',3,1,0,'Iowa State','https://courses.elo.iastate.edu/fall/2015/AER%20E/468/XE/overview'),('ICP469','Artificial Intelligence and Expert Systems',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP481','Lean Production Systems',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP483','Production Planning and Control',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP485','Occupational Ergonomics',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP486','Logistics & Supply Chain Systems',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP487','Occupational Safety and Health',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP495','Design for Manufacturability',3,1,14,'Bradley University','https://www.bradley.edu/academic/departments/imet/programs/syllabi/index.dot#IESyllabi'),('ICP500','Machine Learning',0,1,11,'Coursera','https://www.coursera.org/learn/machine-learning'),('ICP600','Introduction to the Internet of Things',0,1,0,'edX','https://www.edx.org/course/introduction-to-the-internet-of-things-iot'),('ICP620','IoT Sensors and Devices',0,1,0,'edX','https://www.edx.org/course/iot-sensors-devices-curtinx-iot2x'),('ICP640','IoT Networks and Protocols',0,1,0,'edX','https://www.edx.org/course/iot-networks-protocols-curtinx-iot3x'),('ICP660','IoT Programming and Big Data',0,1,0,'edX','https://www.edx.org/course/iot-programming-big-data-curtinx-iot4x'),('ICP680','Enabling Technologies for Data Science and Analytics: The Internet of Things',0,1,0,'edX','https://www.edx.org/course/enabling-technologies-data-science-columbiax-ds103x-1'),('ICP710','Control of Mobile Robots',0,1,0,'Coursera','https://www.coursera.org/learn/mobile-robot'),('ICP720','Robotics: Fundamentals',0,1,0,'edX','https://www.edx.org/micromasters/pennx-robotics'),('ICP740','Robotics: Vision Intelligence and Machine Learning',0,1,7,'edX','https://www.edx.org/course/robotics-vision-intelligence-machine-pennx-robo2x'),('ICP800','Industry 4.0: How to Revolutionize your Business',0,1,6,'edX','https://www.edx.org/course/industry-40-how-to-revolutionize-your-business'),('ICP801','Emotional Intelligence at Work',0,1,2,'Future learn','https://www.futurelearn.com/courses/emotional-intelligence-at-work'),('ICP802','Basics of Network Security - Knowlwdge - Cyber security',0,1,2,'Future learn','https://www.futurelearn.com/courses/network-security-basics'),('ICP803','Robotic Vision: Processing Images',0,1,4,'Future learn','https://www.futurelearn.com/courses/robotic-vision-processing-images'),('ICP804','Big Data: Mathematical Modelling',0,1,3,'Future learn','https://www.futurelearn.com/courses/big-data-mathematical-modelling#section-requirements'),('ICP805','Data Mining with Weka - Pre - Basic statistics',0,1,5,'Future learn','https://www.futurelearn.com/courses/data-mining-with-weka#section-requirements'),('ICP806','Cyber Security: Safety at Home, Online, in Life - Pre - basic cybersecurity',0,1,3,'Future learn','https://www.futurelearn.com/courses/cyber-security#section-requirements'),('ICP807','Building a Future with Robots',0,1,3,'Future learn','https://www.futurelearn.com/courses/robotic-future#section-requirements'),('ICP808','Big Data: Statistical Inference and Machine Learning',0,1,3,'Future learn','https://www.futurelearn.com/courses/big-data-machine-learning#section-requirements'),('ICP809','More Data Mining with Weka - Pre - Data mining with weka',0,1,5,'Future learn','https://www.futurelearn.com/courses/more-data-mining-with-weka'),('ICP810','Introduction to Additive Manufacturing ',0,1,1,'tollingu','https://www.toolingu.com/class/510010/introduction-to-additive-manufacturing-111'),('ICP811','Concepts of Robot Programming',0,1,1,'tollingu','https://www.toolingu.com/class/620260/concepts-of-robot-programming-341'),('ICP812','Robot Sensors',0,1,1,'tollingu','https://www.toolingu.com/class/470150/robot-sensors-150'),('ICP813','Vision Systems',0,1,1,'tollingu','https://www.toolingu.com/class/470250/vision-systems-250'),('ICP814','Overview of PLC Registers',0,1,1,'tollingu','https://www.toolingu.com/class/450305/overview-of-plc-registers-305'),('ICP815','PLC Program Control Instructions',0,1,1,'tollingu','https://www.toolingu.com/class/450310/plc-program-control-instructions-310'),('ICP816','PLC Timers and Counters',0,1,1,'tollingu','https://www.toolingu.com/class/450260/plc-timers-and-counters-260'),('ICP817','Linear Algebra - Foundations to Frontiers',0,1,15,'edX','https://www.edx.org/course/linear-algebra-foundations-to-frontiers-0');
/*!40000 ALTER TABLE `courselist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `initialgoalknowledge`
--

DROP TABLE IF EXISTS `initialgoalknowledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `initialgoalknowledge` (
  `transactionID` bigint(20) NOT NULL,
  `iorg` varchar(10) NOT NULL,
  `knowledgeID` varchar(10) NOT NULL,
  PRIMARY KEY (`transactionID`,`iorg`,`knowledgeID`),
  KEY `knowledgeID` (`knowledgeID`),
  CONSTRAINT `initialgoalknowledge_ibfk_1` FOREIGN KEY (`knowledgeID`) REFERENCES `knowledgelist` (`knowledgeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `initialgoalknowledge`
--

LOCK TABLES `initialgoalknowledge` WRITE;
/*!40000 ALTER TABLE `initialgoalknowledge` DISABLE KEYS */;
INSERT INTO `initialgoalknowledge` VALUES (8889,'I','k1C001'),(8889,'G','k1C620'),(8889,'I','k1T000');
/*!40000 ALTER TABLE `initialgoalknowledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `knowledgeforcareerpath`
--

DROP TABLE IF EXISTS `knowledgeforcareerpath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knowledgeforcareerpath` (
  `careerPathID` char(4) NOT NULL,
  `knowledgeID` char(6) NOT NULL,
  PRIMARY KEY (`careerPathID`,`knowledgeID`),
  KEY `knowledgeID` (`knowledgeID`),
  CONSTRAINT `knowledgeforcareerpath_ibfk_1` FOREIGN KEY (`knowledgeID`) REFERENCES `knowledgelist` (`knowledgeID`),
  CONSTRAINT `knowledgeforcareerpath_ibfk_2` FOREIGN KEY (`careerPathID`) REFERENCES `careerpath` (`careerPathID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knowledgeforcareerpath`
--

LOCK TABLES `knowledgeforcareerpath` WRITE;
/*!40000 ALTER TABLE `knowledgeforcareerpath` DISABLE KEYS */;
INSERT INTO `knowledgeforcareerpath` VALUES ('C100','k1C311'),('C100','k1C412'),('C100','k1C469'),('C100','k1C500');
/*!40000 ALTER TABLE `knowledgeforcareerpath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `knowledgelist`
--

DROP TABLE IF EXISTS `knowledgelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knowledgelist` (
  `knowledgeID` varchar(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`knowledgeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knowledgelist`
--

LOCK TABLES `knowledgelist` WRITE;
/*!40000 ALTER TABLE `knowledgelist` DISABLE KEYS */;
INSERT INTO `knowledgelist` VALUES ('k1C001','Calculus 1'),('k1C002','Calculus 2'),('k1C003','Calculus 3'),('k1C004','Physics 1'),('k1C005','Physics 2'),('k1C006','Introduction to Linear Algebra'),('k1C100','Introduction to Geometry'),('k1C103','Computer Aided Graphics'),('k1C110','Introduction to Computers & Computational Analysis'),('k1C200','Introduction to Computing Using Python'),('k1C222','Design & Analysis Methods for System Improvements'),('k1C248','Engineering System Design Manufacturing Processes and Specifications'),('k1C271','Applied Ergonomics and Work Design'),('k1C300','Introduction to Statistics'),('k1C301','Engineering Economy I'),('k1C302','Introduction to Quality Engineering'),('k1C305','Engineering Economy II'),('k1C311','Introduction to Engineering Statistical Methods'),('k1C313','Operations Research I'),('k1C314','Operations Research II'),('k1C331','Fundamentals of Materials Science'),('k1C333',' Materials Science Laboratory'),('k1C341','Introduction to Manufacturing Processes'),('k1C342','Production Systems'),('k1C348','Solidification Processes'),('k1C361','Introduction to Simulation and Expert Systems'),('k1C385','Introduction to Logistics and Supply Chain'),('k1C386','Industrial and Managerial Engineering'),('k1C395','Solid Modeling & Rapid Prototyping'),('k1C400','Autonomous Navigation for Flying Robots'),('k1C403','Introduction to Sustainable Production Systems'),('k1C412','Design and Analysis of Experiments'),('k1C422','Manufacturing Quality Control'),('k1C431','Materials Engineering'),('k1C432','Industrial Automation'),('k1C441','Industrial Engineering Design'),('k1C443','Manufacturing Processes II'),('k1C445','Computer Aided Manufacturing'),('k1C446','Geometric Variability in Manufacturing'),('k1C447','Biomedical Design and Manufacturing'),('k1C448','Manufacturing Systems Engineering'),('k1C449','Computer Aided Design and Manufacturing'),('k1C461','Simulation of Manufacturing and Service Systems'),('k1C465','Multidisciplinary Engineering Design I'),('k1C466','Facilities Planning'),('k1C467','Multidisciplinary Engineering Design II'),('k1C468','Large-Scale Complex Engineered Systems'),('k1C469','Artificial Intelligence and Expert Systems'),('k1C481','Lean Production Systems'),('k1C483','Production Planning and Control'),('k1C485','Occupational Ergonomics'),('k1C486','Logistics & Supply Chain Systems'),('k1C487','Occupational Safety and Health'),('k1C495','Design for Manufacturability'),('k1C500','Machine Learning'),('k1C600','Introduction to the Internet of Things'),('k1C620','IoT Sensors and Devices'),('k1C640','IoT Networks and Protocols'),('k1C660','IoT Programming and Big Data'),('k1C680','Enabling Technologies for Data Science and Analytics: The Internet of Things'),('k1C710','Control of Mobile Robots'),('k1C720','Robotics: Fundamentals'),('k1C740','Robotics: Vision Intelligence and Machine Learning'),('k1C813','Robotics: Vision'),('k1C814','PLC timers and counters'),('k1C815','PLC Program Control'),('k1C817','Linear Algebra'),('k1T000','High School Diploma'),('k1T100','Linear Algebra'),('k1T101','Metrology'),('k1T103','System Design'),('k1T104','Drafting Standards'),('k1T105','Applied Geometry'),('k1T106','Orthographic Projection'),('k1T107','AutoCAD'),('k1T108','Physiology'),('k1T109','Work Method'),('k1T110','CAD drawing'),('k1T111','Programming'),('k1T114','Human-Machine Systems'),('k1T116','System Improvement'),('k1T117','Setup Reduction'),('k1T118','Facility Layout'),('k1T119','Inventory Management'),('k1T120','Decision Underuncertainty'),('k1T121','Financial Engineering Basics'),('k1T122','Make-Buy Decisions'),('k1T123','Fixturing'),('k1T124','Tooling'),('k1T125','Manufacturing Process Planning'),('k1T126','Geometric Dimensioning'),('k1T127','Computer Aided Inspection'),('k1T128','Accounting'),('k1T129','Flexible Manufacturing'),('k1T130','Break-Even Analysis'),('k1T131','Transportation Model'),('k1T132','Flow Production'),('k1T133','Application of Engineering Design Problems'),('k1T134','Logistics Demand Forecast'),('k1T135','Corrosion Control'),('k1T136','Wear and Fracture'),('k1T137','Metal Cutting'),('k1T138','Metal Casting'),('k1T139','Non-metallic Molding'),('k1T140','Advanced Simulation'),('k1T141','Functional Anatomy'),('k1T142','Logistics System Implementation'),('k1T143','Occupational Regulations and Standards'),('k1T144','Hazard Analysis and Control'),('k1T150','Probability'),('k1T200','Angle Measurement'),('k1T210','Infinite Series'),('k1T211','Parametric Curves'),('k1T212','Polar Coordinates'),('k1T213','Algebra'),('k1T214','Trigonometry'),('k1T215','Analytic Geometry'),('k1T216','Multiple Integrals'),('k1T250','3D Shapes'),('k1T271','Random Variables'),('k1T272','Distributions'),('k1T273','Linear Regression'),('k1T275','Quality Control'),('k1T276','Optimization'),('k1T277','Mathematical Programming'),('k1T278','Linear Prorgamming'),('k1T279','Sensitivity Analysis'),('k1T280','Network Models'),('k1T281','Production Systems'),('k1T282','Forecasting'),('k1T283','Project Planning Scheduling '),('k1T284','Just-in-Time'),('k1T285','Global Supply Chains'),('k1T286','Simulation'),('k1T287','Aplication of Manufacturing and Service Systems'),('k1T288','Fitting Data to Statistical Distributions'),('k1T289','Markov Processes'),('k1T300','Data Structures'),('k1T301','Statistics'),('k1T325','Electric Force'),('k1T326','Electrical Current'),('k1T327','Magnetic Force'),('k1T328','Ray Optics and Image Formation'),('k1T329','Electrical Circuit Theory'),('k1T330','Industrial Control Systems'),('k1T331','Transducers in the Form of Sensors and Actuators'),('k1T332','Material Classification'),('k1T333','Atomic Structure'),('k1T334','Iron-Carbon System'),('k1T335','Material Structure'),('k1T336','Metallographic Examination'),('k1T337','Thermal Treatment'),('k1T342','Machining'),('k1T343','Casting'),('k1T344','Welding'),('k1T345','Measurement'),('k1T346','Injection Molding'),('k1T350','Control Structures'),('k1T353','Representation and Interpretation of Curves, Surfaces, and Solids'),('k1T354','CAD'),('k1T356','Computer Numerical Control'),('k1T357','CNC Programming Languages'),('k1T359','Application of CAD, CAM and FEM'),('k1T360','Sustainability in Production Planning and Inverntory'),('k1T361','Project Scheduling'),('k1T362','Cost Estimating'),('k1T363','Manufacturing Processes'),('k1T364','Closed-loop Supply Chain'),('k1T396','SolidWorks'),('k1T397','Rapid Prototyping'),('k1T400','Algorithm Development'),('k1T423','Quality Function Deployment'),('k1T424','Reliability'),('k1T425','Control Charts'),('k1T426','Nanometarials'),('k1T427','Forming'),('k1T428','Modern Manufacturing Practices'),('k1T429','Assesment and Control of Geometric Variability in Manufacturing Processes of Composites'),('k1T430','Assesment and Control of Geometric Variability in Metal Casting'),('k1T431','Assesment and Control of Geometric Variability in Welding'),('k1T432','Assesment and Control of Geometric Variability in Machining'),('k1T433','Assesment and Control of Geometric Variability in Powder Metallurgy'),('k1T434','Assesment and Control of Geometric Variability in Additive Processing'),('k1T435','Design of Component'),('k1T436','Inspection Methodology'),('k1T437','Process Plan'),('k1T446','CNC Programming'),('k1T447','MasterCAM'),('k1T448','CAD/CAM'),('k1T449','Body Mechanics'),('k1T450','Hypothesis Testing'),('k1T451','Ethics'),('k1T458','Detail Design'),('k1T467','Plant Layout'),('k1T468','Material Handling'),('k1T469','Computer Aided Layout'),('k1T470','k-mean Clustering Algorithm'),('k1T471','Association Rule Mining'),('k1T472','Automated Planning'),('k1T480','Cyber security'),('k1T482','Lean Manufacturing'),('k1T483','Cellular Manufacturing'),('k1T484','Kanban System'),('k1T485','Concurrent Engineering'),('k1T486','Material Resource Planning'),('k1T487','Demand Forecasting'),('k1T488','PERT CPM'),('k1T489','Aggregated Planning'),('k1T490','Inventory Control'),('k1T496','Design Specification'),('k1T497','Robust Design'),('k1T500','Confidence Intervals'),('k1T550','Regression'),('k1T597','Design Large Scale Complex Systems'),('k1T598','Design Reliability'),('k1T599','Product Robustness'),('k1T600','3D Geometry'),('k1T601','Economic Factors for the Formation of a Value Function'),('k1T621','IoT Sensors'),('k1T622','IoT Devices'),('k1T650','Visual Odometry'),('k1T661','IoT Programming'),('k1T662','IoT Data'),('k1T670','Polymer Processing'),('k1T671','Powder Metallurrgy'),('k1T672','Composite Manufacturing '),('k1T681','IoT Networks'),('k1T682','IoT Protocols'),('k1T683','Feature Extraction'),('k1T684','Content Classification'),('k1T700','Linear Control'),('k1T711','Mobile Robots'),('k1T712','Linear System'),('k1T713','Control Design'),('k1T721','Robot Programming'),('k1T741','Image Convolution'),('k1T742','Edge Detection'),('k1T743','Optical Flow Estimation'),('k1T744','Image Morphing'),('k1T745','Image Blending'),('k1T746','Image Carving'),('k1T747','Object Recognition'),('k1T748','Convolutional Neural Network'),('k1T749','Clustering'),('k1T750','Neural Networks'),('k1T751','Robotic Design'),('k1T752','Cloud computing'),('k1T753','Robotics: Sensors'),('k1T754','PLC Programming'),('k1T755','MCR Programming'),('k1T756','MCR Programming'),('k1T757','Cyber security'),('k1T800','Support Vector Machines'),('k1T850','Anomaly Detection'),('k1T900','Unsupervised Learning'),('k1T901','Structure and Properties of Engineering Materials'),('k1T902','Ergonomics'),('k1T903','Work Design'),('k1T904','Productivity'),('k1T905','Statistical Process Control'),('k1T906','Programmable Logic Controllers (PLC)'),('k1T907','Radio Frequency Identification (RFID)'),('k1T908','Bar Coding Technologie'),('k1T909','Design Methodologies'),('k1T910','Material Recovery, Recycling'),('k1T911','Remanufacturing'),('k1T912','Sustainability Rubrics'),('k1T913','Prototype Development'),('k1T914','Fundametals of Ceramic'),('k1T915','Composite Materials'),('k1T916','Biomaterials'),('k1T917','Biomedical Manufacturing'),('k1T918','Utility Theory'),('k1T919','Machining Processes'),('k1T920','Elemantary Mechanics');
/*!40000 ALTER TABLE `knowledgelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postknowledge`
--

DROP TABLE IF EXISTS `postknowledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postknowledge` (
  `courseID` varchar(10) NOT NULL,
  `knowledgeID` varchar(10) NOT NULL,
  PRIMARY KEY (`courseID`,`knowledgeID`),
  KEY `knowledgeID` (`knowledgeID`),
  CONSTRAINT `postknowledge_ibfk_1` FOREIGN KEY (`courseID`) REFERENCES `courselist` (`courseID`),
  CONSTRAINT `postknowledge_ibfk_2` FOREIGN KEY (`knowledgeID`) REFERENCES `knowledgelist` (`knowledgeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postknowledge`
--

LOCK TABLES `postknowledge` WRITE;
/*!40000 ALTER TABLE `postknowledge` DISABLE KEYS */;
INSERT INTO `postknowledge` VALUES ('ICP001','k1C001'),('ICP002','k1C002'),('ICP003','k1C003'),('ICP004','k1C004'),('ICP005','k1C005'),('ICP006','k1C006'),('ICP100','k1C100'),('ICP103','k1C103'),('ICP110','k1C110'),('ICP200','k1C200'),('ICP222','k1C222'),('ICP248','k1C248'),('ICP271','k1C271'),('ICP300','k1C300'),('ICP301','k1C301'),('ICP302','k1C302'),('ICP305','k1C305'),('ICP311','k1C311'),('ICP313','k1C313'),('ICP314','k1C314'),('ICP331','k1C331'),('ICP333','k1C333'),('ICP341','k1C341'),('ICP342','k1C342'),('ICP348','k1C348'),('ICP361','k1C361'),('ICP385','k1C385'),('ICP386','k1C386'),('ICP395','k1C395'),('ICP400','k1C400'),('ICP403','k1C403'),('ICP412','k1C412'),('ICP422','k1C422'),('ICP431','k1C431'),('ICP432','k1C432'),('ICP441','k1C441'),('ICP443','k1C443'),('ICP445','k1C445'),('ICP446','k1C446'),('ICP447','k1C447'),('ICP448','k1C448'),('ICP449','k1C449'),('ICP461','k1C461'),('ICP465','k1C465'),('ICP466','k1C466'),('ICP467','k1C467'),('ICP468','k1C468'),('ICP469','k1C469'),('ICP481','k1C481'),('ICP483','k1C483'),('ICP485','k1C485'),('ICP486','k1C486'),('ICP487','k1C487'),('ICP495','k1C495'),('ICP500','k1C500'),('ICP600','k1C600'),('ICP620','k1C620'),('ICP640','k1C640'),('ICP660','k1C660'),('ICP680','k1C680'),('ICP710','k1C710'),('ICP720','k1C720'),('ICP740','k1C740'),('ICP815','k1C815'),('ICP817','k1C817'),('ICP000','k1T000'),('ICP006','k1T100'),('ICP248','k1T101'),('ICP248','k1T103'),('ICP103','k1T104'),('ICP103','k1T105'),('ICP103','k1T106'),('ICP103','k1T107'),('ICP271','k1T108'),('ICP271','k1T109'),('ICP248','k1T110'),('ICP110','k1T111'),('ICP271','k1T114'),('ICP222','k1T116'),('ICP222','k1T117'),('ICP222','k1T118'),('ICP448','k1T118'),('ICP222','k1T119'),('ICP448','k1T123'),('ICP446','k1T124'),('ICP448','k1T124'),('ICP448','k1T125'),('ICP448','k1T126'),('ICP448','k1T127'),('ICP305','k1T128'),('ICP448','k1T129'),('ICP385','k1T131'),('ICP448','k1T132'),('ICP441','k1T133'),('ICP385','k1T134'),('ICP431','k1T135'),('ICP431','k1T136'),('ICP441','k1T137'),('ICP443','k1T138'),('ICP443','k1T139'),('ICP461','k1T140'),('ICP485','k1T141'),('ICP486','k1T142'),('ICP487','k1T143'),('ICP487','k1T144'),('ICP302','k1T150'),('ICP311','k1T150'),('ICP100','k1T200'),('ICP100','k1T250'),('ICP447','k1T275'),('ICP465','k1T275'),('ICP342','k1T281'),('ICP342','k1T282'),('ICP342','k1T283'),('ICP342','k1T284'),('ICP342','k1T285'),('ICP361','k1T286'),('ICP200','k1T300'),('ICP302','k1T301'),('ICP311','k1T301'),('ICP432','k1T329'),('ICP432','k1T330'),('ICP432','k1T331'),('ICP331','k1T332'),('ICP331','k1T333'),('ICP331','k1T334'),('ICP331','k1T335'),('ICP333','k1T336'),('ICP333','k1T337'),('ICP341','k1T342'),('ICP341','k1T343'),('ICP348','k1T343'),('ICP341','k1T344'),('ICP348','k1T344'),('ICP341','k1T345'),('ICP341','k1T346'),('ICP200','k1T350'),('ICP449','k1T353'),('ICP449','k1T354'),('ICP449','k1T356'),('ICP449','k1T357'),('ICP465','k1T359'),('ICP467','k1T359'),('ICP403','k1T360'),('ICP465','k1T361'),('ICP465','k1T362'),('ICP465','k1T363'),('ICP403','k1T364'),('ICP395','k1T396'),('ICP348','k1T397'),('ICP395','k1T397'),('ICP467','k1T397'),('ICP200','k1T400'),('ICP422','k1T423'),('ICP422','k1T424'),('ICP422','k1T425'),('ICP441','k1T427'),('ICP446','k1T429'),('ICP446','k1T430'),('ICP446','k1T431'),('ICP446','k1T432'),('ICP446','k1T433'),('ICP446','k1T434'),('ICP446','k1T435'),('ICP446','k1T436'),('ICP446','k1T437'),('ICP445','k1T446'),('ICP445','k1T447'),('ICP445','k1T448'),('ICP449','k1T448'),('ICP447','k1T449'),('ICP300','k1T450'),('ICP447','k1T451'),('ICP467','k1T458'),('ICP466','k1T467'),('ICP466','k1T468'),('ICP466','k1T469'),('ICP469','k1T470'),('ICP469','k1T471'),('ICP469','k1T472'),('ICP222','k1T482'),('ICP448','k1T482'),('ICP481','k1T482'),('ICP448','k1T483'),('ICP481','k1T483'),('ICP481','k1T484'),('ICP465','k1T485'),('ICP481','k1T485'),('ICP342','k1T486'),('ICP483','k1T486'),('ICP483','k1T487'),('ICP483','k1T488'),('ICP483','k1T489'),('ICP342','k1T490'),('ICP483','k1T490'),('ICP495','k1T496'),('ICP495','k1T497'),('ICP300','k1T500'),('ICP300','k1T550'),('ICP468','k1T597'),('ICP468','k1T598'),('ICP468','k1T599'),('ICP400','k1T600'),('ICP468','k1T601'),('ICP620','k1T621'),('ICP620','k1T622'),('ICP400','k1T650'),('ICP660','k1T661'),('ICP660','k1T662'),('ICP348','k1T670'),('ICP348','k1T671'),('ICP348','k1T672'),('ICP640','k1T681'),('ICP680','k1T681'),('ICP640','k1T682'),('ICP680','k1T682'),('ICP680','k1T683'),('ICP680','k1T684'),('ICP400','k1T700'),('ICP710','k1T711'),('ICP710','k1T712'),('ICP710','k1T713'),('ICP720','k1T721'),('ICP740','k1T741'),('ICP740','k1T742'),('ICP740','k1T743'),('ICP740','k1T744'),('ICP740','k1T745'),('ICP740','k1T746'),('ICP740','k1T747'),('ICP740','k1T748'),('ICP500','k1T750'),('ICP500','k1T800'),('ICP500','k1T850'),('ICP500','k1T900'),('ICP271','k1T903'),('ICP485','k1T903'),('ICP271','k1T904'),('ICP432','k1T906'),('ICP432','k1T907'),('ICP432','k1T908'),('ICP465','k1T909'),('ICP403','k1T910'),('ICP403','k1T911'),('ICP403','k1T912'),('ICP465','k1T913'),('ICP447','k1T917'),('ICP468','k1T918'),('ICP248','k1T919');
/*!40000 ALTER TABLE `postknowledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preknowledge`
--

DROP TABLE IF EXISTS `preknowledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preknowledge` (
  `courseID` varchar(10) NOT NULL,
  `knowledgeID` varchar(10) NOT NULL,
  PRIMARY KEY (`courseID`,`knowledgeID`),
  KEY `knowledgeID` (`knowledgeID`),
  CONSTRAINT `preknowledge_ibfk_1` FOREIGN KEY (`courseID`) REFERENCES `courselist` (`courseID`),
  CONSTRAINT `preknowledge_ibfk_2` FOREIGN KEY (`knowledgeID`) REFERENCES `knowledgelist` (`knowledgeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preknowledge`
--

LOCK TABLES `preknowledge` WRITE;
/*!40000 ALTER TABLE `preknowledge` DISABLE KEYS */;
INSERT INTO `preknowledge` VALUES ('ICP002','k1C001'),('ICP110','k1C001'),('ICP301','k1C001'),('ICP302','k1C001'),('ICP386','k1C001'),('ICP003','k1C002'),('ICP311','k1C002'),('ICP740','k1C002'),('ICP817','k1C002'),('ICP313','k1C003'),('ICP005','k1C004'),('ICP331','k1C005'),('ICP807','k1C005'),('ICP400','k1C100'),('ICP341','k1C103'),('ICP395','k1C103'),('ICP361','k1C110'),('ICP469','k1C110'),('ICP620','k1C110'),('ICP680','k1C110'),('ICP710','k1C110'),('ICP720','k1C110'),('ICP400','k1C200'),('ICP802','k1C200'),('ICP432','k1C222'),('ICP222','k1C248'),('ICP348','k1C248'),('ICP441','k1C248'),('ICP448','k1C248'),('ICP449','k1C248'),('ICP222','k1C271'),('ICP441','k1C271'),('ICP805','k1C300'),('ICP305','k1C301'),('ICP481','k1C301'),('ICP361','k1C311'),('ICP412','k1C311'),('ICP422','k1C311'),('ICP483','k1C311'),('ICP485','k1C311'),('ICP486','k1C311'),('ICP804','k1C311'),('ICP314','k1C313'),('ICP483','k1C313'),('ICP486','k1C313'),('ICP333','k1C331'),('ICP441','k1C331'),('ICP431','k1C333'),('ICP403','k1C341'),('ICP441','k1C341'),('ICP445','k1C341'),('ICP481','k1C341'),('ICP495','k1C341'),('ICP815','k1C341'),('ICP446','k1C348'),('ICP461','k1C361'),('ICP466','k1C386'),('ICP481','k1C386'),('ICP483','k1C386'),('ICP445','k1C395'),('ICP495','k1C395'),('ICP740','k1C500'),('ICP815','k1C620'),('ICP740','k1C720'),('ICP811','k1C720'),('ICP816','k1C813'),('ICP815','k1C817'),('ICP001','k1T000'),('ICP004','k1T000'),('ICP006','k1T000'),('ICP103','k1T000'),('ICP110','k1T000'),('ICP301','k1T000'),('ICP465','k1T000'),('ICP467','k1T000'),('ICP468','k1T000'),('ICP487','k1T000'),('ICP600','k1T000'),('ICP400','k1T100'),('ICP500','k1T100'),('ICP740','k1T100'),('ICP500','k1T111'),('ICP300','k1T150'),('ICP300','k1T301'),('ICP500','k1T301'),('ICP808','k1T301'),('ICP803','k1T721 ');
/*!40000 ALTER TABLE `preknowledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `transactionID` bigint(20) NOT NULL,
  `stage` int(11) NOT NULL,
  `courseID` varchar(10) NOT NULL,
  PRIMARY KEY (`transactionID`,`stage`,`courseID`),
  KEY `courseID` (`courseID`),
  CONSTRAINT `result_ibfk_1` FOREIGN KEY (`courseID`) REFERENCES `courselist` (`courseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1288,1,'ICP001'),(1509,1,'ICP001'),(7645,1,'ICP001'),(9524,1,'ICP001'),(1509,2,'ICP002'),(5461,1,'ICP002'),(6325,1,'ICP002'),(7645,2,'ICP002'),(9524,2,'ICP002'),(1851,1,'ICP003'),(2474,1,'ICP003'),(2610,1,'ICP003'),(2779,1,'ICP003'),(4375,1,'ICP003'),(6325,2,'ICP003'),(6679,1,'ICP003'),(9076,1,'ICP004'),(9076,2,'ICP005'),(1031,1,'ICP006'),(1296,1,'ICP006'),(1509,1,'ICP006'),(1620,1,'ICP006'),(1646,1,'ICP006'),(2598,1,'ICP006'),(3286,1,'ICP006'),(3948,1,'ICP006'),(4607,1,'ICP006'),(4980,1,'ICP006'),(5461,1,'ICP006'),(5779,1,'ICP006'),(5814,1,'ICP006'),(6294,1,'ICP006'),(7056,1,'ICP006'),(7326,1,'ICP006'),(7420,1,'ICP006'),(7645,1,'ICP006'),(7720,1,'ICP006'),(7797,1,'ICP006'),(7817,1,'ICP006'),(7821,1,'ICP006'),(7831,1,'ICP006'),(7832,1,'ICP006'),(8062,1,'ICP006'),(8306,1,'ICP006'),(9050,1,'ICP006'),(9249,1,'ICP006'),(9306,1,'ICP006'),(9451,1,'ICP006'),(9524,1,'ICP006'),(9814,1,'ICP006'),(1133,1,'ICP103'),(1296,1,'ICP103'),(1572,1,'ICP103'),(2280,1,'ICP103'),(2711,1,'ICP103'),(2985,1,'ICP103'),(3667,1,'ICP103'),(3668,1,'ICP103'),(3705,1,'ICP103'),(4934,1,'ICP103'),(4978,1,'ICP103'),(5450,1,'ICP103'),(5497,1,'ICP103'),(5966,1,'ICP103'),(6437,1,'ICP103'),(6604,1,'ICP103'),(6898,1,'ICP103'),(7196,1,'ICP103'),(7227,1,'ICP103'),(7769,1,'ICP103'),(8628,1,'ICP103'),(8647,1,'ICP103'),(8688,1,'ICP103'),(8941,1,'ICP103'),(8952,1,'ICP103'),(8959,1,'ICP103'),(9797,1,'ICP103'),(9968,1,'ICP103'),(1031,1,'ICP110'),(1136,1,'ICP110'),(1288,2,'ICP110'),(1291,1,'ICP110'),(1293,1,'ICP110'),(1296,1,'ICP110'),(1325,1,'ICP110'),(1355,1,'ICP110'),(1406,1,'ICP110'),(1509,2,'ICP110'),(1572,1,'ICP110'),(1620,1,'ICP110'),(1646,1,'ICP110'),(1981,1,'ICP110'),(2598,1,'ICP110'),(3286,1,'ICP110'),(3948,1,'ICP110'),(4281,1,'ICP110'),(4607,1,'ICP110'),(4775,1,'ICP110'),(4980,1,'ICP110'),(5450,1,'ICP110'),(5461,1,'ICP110'),(5779,1,'ICP110'),(5814,1,'ICP110'),(5897,1,'ICP110'),(6294,1,'ICP110'),(6974,1,'ICP110'),(7056,1,'ICP110'),(7227,1,'ICP110'),(7326,1,'ICP110'),(7645,2,'ICP110'),(7797,1,'ICP110'),(7821,1,'ICP110'),(7832,1,'ICP110'),(8062,1,'ICP110'),(8306,1,'ICP110'),(8527,1,'ICP110'),(8889,1,'ICP110'),(9050,1,'ICP110'),(9117,1,'ICP110'),(9249,1,'ICP110'),(9306,1,'ICP110'),(9451,1,'ICP110'),(9524,2,'ICP110'),(9784,1,'ICP110'),(9814,1,'ICP110'),(1607,1,'ICP301'),(3667,1,'ICP301'),(3668,1,'ICP301'),(3705,1,'ICP301'),(3969,1,'ICP301'),(5417,1,'ICP301'),(5966,1,'ICP301'),(6437,1,'ICP301'),(7227,1,'ICP301'),(7593,1,'ICP301'),(8628,1,'ICP301'),(8688,1,'ICP301'),(9968,1,'ICP301'),(4607,1,'ICP302'),(5461,1,'ICP302'),(5779,1,'ICP302'),(7797,1,'ICP302'),(9306,1,'ICP302'),(1031,1,'ICP311'),(1141,1,'ICP311'),(1293,1,'ICP311'),(1296,1,'ICP311'),(1509,3,'ICP311'),(1620,1,'ICP311'),(1646,1,'ICP311'),(1733,1,'ICP311'),(1851,1,'ICP311'),(1981,1,'ICP311'),(2474,1,'ICP311'),(2598,1,'ICP311'),(2610,1,'ICP311'),(2779,1,'ICP311'),(3286,1,'ICP311'),(3948,1,'ICP311'),(4375,1,'ICP311'),(4747,1,'ICP311'),(4980,1,'ICP311'),(5814,1,'ICP311'),(6294,1,'ICP311'),(6679,1,'ICP311'),(7056,1,'ICP311'),(7159,1,'ICP311'),(7227,1,'ICP311'),(7326,1,'ICP311'),(7420,1,'ICP311'),(7645,3,'ICP311'),(7720,1,'ICP311'),(7817,1,'ICP311'),(7821,1,'ICP311'),(7831,1,'ICP311'),(7832,1,'ICP311'),(8062,1,'ICP311'),(8306,1,'ICP311'),(9050,1,'ICP311'),(9249,1,'ICP311'),(9451,1,'ICP311'),(9524,3,'ICP311'),(9812,1,'ICP311'),(9814,1,'ICP311'),(9978,1,'ICP311'),(1296,1,'ICP313'),(1851,2,'ICP313'),(2317,1,'ICP313'),(2474,2,'ICP313'),(2565,1,'ICP313'),(2598,1,'ICP313'),(2610,2,'ICP313'),(2779,2,'ICP313'),(4375,2,'ICP313'),(4747,1,'ICP313'),(6679,2,'ICP313'),(6922,1,'ICP313'),(9812,1,'ICP313'),(9978,1,'ICP313'),(2598,1,'ICP331'),(9076,3,'ICP331'),(1133,2,'ICP341'),(1296,2,'ICP341'),(1572,2,'ICP341'),(1607,1,'ICP341'),(2114,1,'ICP341'),(2280,2,'ICP341'),(2317,1,'ICP341'),(2565,1,'ICP341'),(2711,2,'ICP341'),(3667,2,'ICP341'),(3668,2,'ICP341'),(3705,2,'ICP341'),(3969,1,'ICP341'),(4934,2,'ICP341'),(5417,1,'ICP341'),(5450,2,'ICP341'),(5497,2,'ICP341'),(5966,2,'ICP341'),(6437,2,'ICP341'),(6604,2,'ICP341'),(6898,2,'ICP341'),(6922,1,'ICP341'),(7056,1,'ICP341'),(7196,2,'ICP341'),(7227,2,'ICP341'),(7593,1,'ICP341'),(7769,2,'ICP341'),(8628,2,'ICP341'),(8647,2,'ICP341'),(8688,2,'ICP341'),(8941,2,'ICP341'),(8952,2,'ICP341'),(8959,2,'ICP341'),(9185,1,'ICP341'),(9451,1,'ICP341'),(9797,2,'ICP341'),(9968,2,'ICP341'),(1035,1,'ICP386'),(1572,1,'ICP386'),(1583,1,'ICP386'),(1607,1,'ICP386'),(1797,1,'ICP386'),(2026,1,'ICP386'),(2280,1,'ICP386'),(2675,1,'ICP386'),(2711,1,'ICP386'),(3667,1,'ICP386'),(3668,1,'ICP386'),(3705,1,'ICP386'),(3877,1,'ICP386'),(3969,1,'ICP386'),(3971,1,'ICP386'),(5417,1,'ICP386'),(5448,1,'ICP386'),(5450,1,'ICP386'),(5569,1,'ICP386'),(5757,1,'ICP386'),(5966,1,'ICP386'),(6032,1,'ICP386'),(6437,1,'ICP386'),(6604,1,'ICP386'),(6898,1,'ICP386'),(7227,1,'ICP386'),(7297,1,'ICP386'),(7593,1,'ICP386'),(8030,1,'ICP386'),(8628,1,'ICP386'),(8688,1,'ICP386'),(8952,1,'ICP386'),(9325,1,'ICP386'),(9968,1,'ICP386'),(1133,3,'ICP395'),(1296,3,'ICP395'),(1572,2,'ICP395'),(2114,2,'ICP395'),(2280,2,'ICP395'),(2317,2,'ICP395'),(2565,2,'ICP395'),(5450,2,'ICP395'),(6922,2,'ICP395'),(7056,2,'ICP395'),(7196,2,'ICP395'),(8941,2,'ICP395'),(9185,2,'ICP395'),(9451,2,'ICP395'),(1141,2,'ICP422'),(1733,2,'ICP422'),(7159,2,'ICP422'),(1572,3,'ICP445'),(2114,3,'ICP445'),(2317,3,'ICP445'),(2565,3,'ICP445'),(5450,3,'ICP445'),(7056,3,'ICP445'),(9185,3,'ICP445'),(9451,3,'ICP445'),(1035,2,'ICP466'),(1572,2,'ICP466'),(1583,2,'ICP466'),(1797,2,'ICP466'),(2026,2,'ICP466'),(2280,2,'ICP466'),(2675,2,'ICP466'),(3971,2,'ICP466'),(5448,2,'ICP466'),(5450,2,'ICP466'),(5569,2,'ICP466'),(5757,2,'ICP466'),(6032,2,'ICP466'),(7297,2,'ICP466'),(8030,2,'ICP466'),(6874,1,'ICP467'),(8509,1,'ICP467'),(1355,2,'ICP469'),(1646,2,'ICP469'),(4281,2,'ICP469'),(4980,2,'ICP469'),(6974,2,'ICP469'),(9117,2,'ICP469'),(9784,2,'ICP469'),(1607,2,'ICP481'),(2711,3,'ICP481'),(3667,3,'ICP481'),(3668,3,'ICP481'),(3705,3,'ICP481'),(3969,2,'ICP481'),(5417,2,'ICP481'),(5966,3,'ICP481'),(6437,3,'ICP481'),(6604,3,'ICP481'),(6898,3,'ICP481'),(7227,3,'ICP481'),(7593,2,'ICP481'),(8628,3,'ICP481'),(8688,3,'ICP481'),(8952,3,'ICP481'),(9968,3,'ICP481'),(1851,3,'ICP486'),(2474,3,'ICP486'),(2610,3,'ICP486'),(2779,3,'ICP486'),(4375,3,'ICP486'),(4747,2,'ICP486'),(6679,3,'ICP486'),(9812,2,'ICP486'),(9978,2,'ICP486'),(2280,3,'ICP495'),(7196,3,'ICP495'),(8941,3,'ICP495'),(1031,2,'ICP500'),(1293,2,'ICP500'),(1296,2,'ICP500'),(1509,4,'ICP500'),(1620,2,'ICP500'),(1646,2,'ICP500'),(1981,2,'ICP500'),(2598,2,'ICP500'),(3286,2,'ICP500'),(3948,2,'ICP500'),(4607,2,'ICP500'),(4775,2,'ICP500'),(4980,2,'ICP500'),(5461,2,'ICP500'),(5779,2,'ICP500'),(5814,2,'ICP500'),(6294,2,'ICP500'),(7056,2,'ICP500'),(7227,2,'ICP500'),(7326,2,'ICP500'),(7420,2,'ICP500'),(7645,4,'ICP500'),(7720,2,'ICP500'),(7797,2,'ICP500'),(7817,2,'ICP500'),(7821,2,'ICP500'),(7831,2,'ICP500'),(7832,2,'ICP500'),(8062,2,'ICP500'),(8306,2,'ICP500'),(8527,2,'ICP500'),(9050,2,'ICP500'),(9249,2,'ICP500'),(9306,2,'ICP500'),(9451,2,'ICP500'),(9524,4,'ICP500'),(9814,2,'ICP500'),(1572,2,'ICP620'),(5450,2,'ICP620'),(8889,2,'ICP620'),(1325,2,'ICP680'),(2317,1,'ICP680'),(1136,2,'ICP710'),(5897,2,'ICP710'),(1291,2,'ICP720'),(1572,2,'ICP720'),(4980,2,'ICP720'),(5450,2,'ICP720'),(5461,2,'ICP720'),(7056,2,'ICP720'),(7720,1,'ICP720'),(7797,2,'ICP720'),(9249,2,'ICP720'),(9451,2,'ICP720'),(4980,3,'ICP740'),(5461,3,'ICP740'),(7056,3,'ICP740'),(7720,3,'ICP740'),(9451,3,'ICP740'),(5450,3,'ICP815'),(5450,1,'ICP817');
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-19 13:16:53
