from django.apps import AppConfig


class AutoplanConfig(AppConfig):
    name = 'autoplan'
